# Calculate the amount of cost or profit being generated by solar over the day

import datetime
import csv
import matplotlib.pyplot as plt

SOLAR_MULTIPLIER = 1
BATTERY = 0
TITLE = f"{5 * SOLAR_MULTIPLIER}kW solar plus {BATTERY}kWh Battery"

SUPPLY_CHARGE = 1.051400
OFF_PEAK = 0.153645
WEEKEND_SHOULDER = 0.292100
WEEKDAY_SHOULDER = 0.292100
PEAK = 0.557734

# On a weekday it is:
#   off peak from 00:00 to  07:00 and 21:00:01 to 23:59,
#   shoulder from 07:00 to 15:00
#   peak from 15:00:01 to 21:00
# On a weekend it is:
#   off peak from 00:00 to  07:00 and 21:00:01 to 23:59,
#   shoulder from 07:00 to 21:00


# Figure out the appropriate rate based on the time of day for Synergy Smart Home Plan
def calculate_rate(in_dt):
    rate: float = None
    # Off peak crosses midnight so increment end datetime object by one day
    end_dt = in_dt + datetime.timedelta(days=1)
    # Split datetime objects into days/years/months
    start_day = in_dt.day
    start_year = in_dt.year
    start_month = in_dt.month
    end_day = end_dt.day
    end_month = end_dt.month
    end_year = end_dt.year
    # Use day month and year from in_dt to create new datetime objects for start and end of off peak
    off_peak_start = datetime.datetime(day=start_day, month=start_month, year=start_year, hour=21)
    off_peak_end = datetime.datetime(year=end_year, month=end_month, day=end_day, hour=7)
    # Check if it's off peak
    if off_peak_start < in_dt < off_peak_end:
        rate = OFF_PEAK
    else:
        # Because it's not off peak we now need to know if it's shoulder or peak which changes on weekdays vs
        # weekends
        # Express day of the week as an integer 0 = Monday, 6 = Sunday
        day_of_week = in_dt.weekday()
        # Check if it's a weekday
        if day_of_week < 5:
            # Is it weekday shoulder or peak
            shoulder_start = datetime.time(hour=7)
            shoulder_end = datetime.time(hour=15)
            if shoulder_start < in_dt.time() < shoulder_end:
                rate = WEEKDAY_SHOULDER
            else:
                # It's peak, panic!
                rate = PEAK
        else:
            # It must be weekend shoulder, there is no peak on the weekend
            rate = WEEKEND_SHOULDER
    return rate


# Return the current feed in tarrif
def get_solar_profit(exported, in_date):
    # If we don't upgrade we remain on the REBS feed it rate of 7c
    net = exported * 0.071350
    # If we upgrade the system we are forced on the DEBS rate which varies by time
    if SOLAR_MULTIPLIER > 1 or BATTERY > 0:
        # Create time objects marking start and end of DEBS peak rate
        sol_peak_start = datetime.time(hour=15)
        sol_peak_end = datetime.time(hour=21)
        if sol_peak_start < in_date.time() < sol_peak_end:
            # It's peak time
            net = exported * 0.10
        else:
            # It's off peak T_T
            net = exported * 0.0275
    return net


# To draw bands that highlight peak and off peak periods we need datetime objects representing the start and end periods
def get_span(dt, start_hr, end_hr):
    end_dt = dt
    # If we're ending before we're starting we must be crossing midnight
    if end_hr < start_hr:
        # Increment the day by one for the end datetime, this handles month and year boundaries
        end_dt = dt + datetime.timedelta(days=1)
    # Split out day month and year
    start_day = dt.day
    start_year = dt.year
    start_month = dt.month
    end_day = end_dt.day
    end_month = end_dt.month
    end_year = end_dt.year
    # Combine day month and year from current datetime object with the start and end hours of the period
    start = datetime.datetime(start_year, start_month, start_day, start_hr)
    end = datetime.datetime(end_year, end_month, end_day, end_hr)
    return start, end


# TODO: Convert all this to an object
if __name__ == '__main__':
    # Create lists to store incrementing costs
    dates = []
    costs = []
    solar_output = []
    solar_dates = []
    battery_charge = []
    daily_usage = []
    solar_profit = 0
    total_cost = 0
    battery = BATTERY
    # open the csv file for reading
    with open('1yr15min.csv') as csvfile:
        # Create csv handler object for reading file line by line
        data_reader = csv.reader(csvfile)
        # Get the header and discard it
        _header = data_reader.__next__()
        # Get the first row of readings to set the baseline
        date, prev_use, prev_solar = data_reader.__next__()
        # Cast from strings to floats
        prev_use = float(prev_use)
        prev_solar = float(prev_solar)
        # Track day changes to add supply charge
        prev_day = datetime.datetime.fromtimestamp(int(date)).day
        daily_export = 0
        daily_solar = 0
        daily_use = 0
        # Step line by line through the CSV file
        for date, use, solar in data_reader:
            # Check for null values
            if use.find('null') == -1 and solar.find('null') == -1:
                # Quantise each reading into energy over that 15min period
                use = float(use)
                solar = float(solar)
                use_delta = use - prev_use
                prev_use = use
                solar_delta = solar - prev_solar
                prev_solar = solar
                # Convert string to datetime object
                dt = datetime.datetime.fromtimestamp(int(date))
                cost = 0
                # Isolate usage from solar
                daily_use += use_delta + solar_delta
                # Fantasy number for if we increased our solar
                if SOLAR_MULTIPLIER > 1 and solar_delta > 0:
                    solar_delta = solar_delta * (SOLAR_MULTIPLIER - 1)
                    use_delta -= solar_delta
                daily_solar += solar_delta
                # Are we importing from the grid?
                if use_delta > 0:
                    # Use the battery first
                    if battery > 0:
                        battery -= use_delta
                        if battery < 0:
                            # Battery didn't have enough for all of it. Pull the rest from the grid
                            cost = -battery * calculate_rate(dt)
                        if battery < 0:
                            # Make sure battery can't go negative
                            battery = 0
                    else:
                        # No battery power so pull from the grid and calculate the cost
                        cost = use_delta * calculate_rate(dt)
                else:
                    # Capture our solar exports
                    # Charge battery first
                    if battery < BATTERY:
                        battery -= use_delta
                        if battery > BATTERY:
                            # We had more than the battery could handle, sell the rest to the grid
                            excess = BATTERY - battery
                            cost = get_solar_profit(excess, dt)
                            # print(f"Just made ${-cost:0.2f} from battery excess")
                            solar_profit -= cost
                            daily_export -= excess
                            battery = BATTERY
                    else:
                        if daily_export < 50:
                            cost = get_solar_profit(use_delta, dt)
                            solar_profit -= cost
                            daily_export -= use_delta
                total_cost += cost
                if dt.day != prev_day:
                    total_cost += SUPPLY_CHARGE
                    # print(f"Boom! Have another {SUPPLY_CHARGE}!")
                    prev_day = dt.day
                    # Reset daily export limit
                    if daily_export >= 50:
                        # print("Hit daily export cap")
                        pass
                    daily_export = 0
                    # Daily solar prodiction
                    solar_output.append(daily_solar)
                    solar_dates.append(dt)
                    daily_solar = 0
                    # Log daily usage
                    daily_usage.append(daily_use)
                    daily_use = 0
                dates.append(dt)
                costs.append(total_cost)
                battery_charge.append(battery)
            else:
                print(f"use = {use} and solar = {solar}")
    fig, power_cost = plt.subplots()
    plt.title(TITLE)
    power_cost.plot(dates, costs, 'b-', label='Power Bill')
    plt.legend(loc='lower left')
    solar_kWh = power_cost.twinx()
    solar_kWh.plot(solar_dates, solar_output, 'g-', label='Solar Production')
    solar_kWh.plot(solar_dates, daily_usage, 'r-', label='Daily Usage')
    start_date = dates[0]
    end_date = dates[len(dates)-1]
    delta = end_date - start_date
    day_x = start_date
    for day in range(delta.days+1):
        peak_start, peak_end = get_span(day_x, 15, 21)
        # my_plot.axvspan(peak_start, peak_end, alpha=0.5, color='red')
        off_peak_start, off_peak_end = get_span(day_x, 21, 7)
        # my_plot.axvspan(off_peak_start, off_peak_end, alpha=0.5, color='green')
        day_x += datetime.timedelta(days=1)
    power_cost.set_ylabel('Cost in $')
    solar_kWh.set_ylabel('Solar and Usage in kWh')
    plt.xlabel('Date')
    plt.legend(loc='lower right')
    plt.show()
