This is a tool to use historical data from Open Energy Monitor to predict the impact from adding extra solar or battery capacity.

It works best with regular intervals (tested mostly with 15min).

Data for rates and feed in tariffs taken from Synergys website.
DEBS rate:
https://www.synergy.net.au/Your-home/Help-and-advice/Solar-credits-and-upgrades/What-will-be-the-DEBS-Buyback-rate
https://www.synergy.net.au/Your-home/Energy-plans/Smart-Home-Plan

The REBS rate was taken from my last power bill as I was unable to find it on the Synergy website.
